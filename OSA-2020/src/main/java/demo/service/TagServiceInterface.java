package demo.service;

import java.util.List;

import demo.entity.Tag;

public interface TagServiceInterface {

	Tag getOne(Long id);
	Tag save(Tag tag);
	void delete(Tag tag);
	List<Tag> findAll();
	
	
}
