package demo.service;

import java.util.List;

import demo.entity.Message;

public interface MessageServiceInterface {
	
	Message getOne(Long id);
	Message save(Message message);
	void delete(Message message);
	List<Message> findAll();

}
