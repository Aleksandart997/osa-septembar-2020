package demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.entity.Contact;
import demo.repository.ContactRepository;

@Service
public class ContactService implements ContactServiceInterface{

	@Autowired
	ContactRepository contactRepository;
	
	@Override
	public Contact getOne(Long id) {
		return contactRepository.getOne(id);
	}
	
	@Override
	public List<Contact> findAll() {
		return contactRepository.findAll();
	}
	
	@Override
	public List<Contact> findByFirstname(String firstname) {
		return contactRepository.findByFirstname(firstname);
	}
	
	@Override
	public List<Contact> findByLastName(String lastname){
		return contactRepository.findByLastname(lastname);
	}
	
	@Override
	public Contact findByDisplayname(String displayname) {
		return contactRepository.findByDisplayname(displayname);
	}
	
	@Override
	public Contact findByEmail(String email) {
		return contactRepository.findByEmail(email);
	}
	
	@Override
	public Contact save(Contact contact) {
		return contactRepository.save(contact);
	}
	
	@Override
	public void delete(Contact contact) {
		contactRepository.delete(contact);
	}
}
