package demo.service;

import java.util.List;

import demo.entity.User;

public interface UserServiceInterface {

	
	User getOne(Long id);
	User findByUsername(String username);
	User findByUsernameAndPassword(String username, String password);
	List<User> findByFirstname(String firstname);
	List<User> findByLastname(String lastname);
	List<User> findAll();
	User save(User user);
	void delete(User user);
	
}
