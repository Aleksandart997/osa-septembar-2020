package demo.service;

import java.util.List;

import demo.entity.Attachment;

public interface AttachmentServiceInterface {

	Attachment getOne(Long id);
	Attachment save(Attachment attachment);
	void delete(Attachment attachment);
	List<Attachment> findAll();
}
