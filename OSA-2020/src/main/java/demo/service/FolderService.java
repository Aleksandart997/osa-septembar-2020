 package demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.entity.Account;
import demo.entity.Folder;
import demo.repository.FolderRepository;

@Service
public class FolderService {
	

	@Autowired
	FolderRepository folderRepository;
	
	public Folder getOne(Long id) {
		return folderRepository.getOne(id);
	}
	
	public Folder findByName(String name) {
		return folderRepository.findByName(name);
	}
	
	public Folder findByAccount(Account account) {
		return folderRepository.findByAccount(account);
	}
	
	public Folder save(Folder folder) {
		return folderRepository.save(folder);
	}

	public void delete(Folder folder) {
		folderRepository.delete(folder);
	}
	
	public List<Folder> findAll() {
		return folderRepository.findAll();
	}

}
