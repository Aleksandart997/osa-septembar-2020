package demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.entity.Contact;
import demo.entity.Photo;
import demo.repository.PhotoRepository;

@Service
public class PhotoService {

	@Autowired
	PhotoRepository photoRepository;
	
	public Photo getOne(Long id) {
		return photoRepository.getOne(id);
	}
	
	public List<Photo> findAll(){
		return photoRepository.findAll();
	}
	
	public List<Photo> findByContact(Contact contact){
		return photoRepository.findByContact(contact);
	}
	
	public Photo save(Photo photo) {
		return photoRepository.save(photo);
	}
	
	public void delete(Photo photo) {
		photoRepository.delete(photo);
	}
}
