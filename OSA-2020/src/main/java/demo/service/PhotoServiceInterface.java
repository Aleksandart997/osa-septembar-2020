package demo.service;

import java.util.List;

import demo.entity.Contact;
import demo.entity.Photo;

public interface PhotoServiceInterface {
	
	Photo getOne(Long id);
	List<Photo> findAll();
	List<Photo> findByContact(Contact contact);
	Photo save(Photo photo);
	void delete(Photo photo);

}
