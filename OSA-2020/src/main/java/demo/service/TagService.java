package demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.entity.Tag;
import demo.repository.TagRepository;

@Service
public class TagService implements TagServiceInterface {
	
	@Autowired
	TagRepository tagRepository;
	
	@Override
	public Tag getOne(Long id) {
		return tagRepository.getOne(id);
	}
	
	@Override
	public Tag save(Tag tag) {
		return tagRepository.save(tag);
	}

	@Override
	public void delete(Tag tag) {
		tagRepository.delete(tag);
	}
	
	@Override
	public List<Tag> findAll() {
		return tagRepository.findAll();
	}

}
