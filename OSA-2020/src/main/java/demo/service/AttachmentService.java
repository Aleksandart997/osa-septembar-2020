package demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.entity.Attachment;
import demo.repository.AttachmentRepository;

@Service
public class AttachmentService implements AttachmentServiceInterface{
	

	@Autowired
	AttachmentRepository attachmentRepository;
	
	@Override
	public Attachment getOne(Long id) {
		return attachmentRepository.getOne(id);
	}
	
	@Override
	public Attachment save(Attachment attachment) {
		return attachmentRepository.save(attachment);
	}
	
	@Override
	public void delete(Attachment attachment) {
		attachmentRepository.delete(attachment);
	}
	
	@Override
	public List<Attachment> findAll() {
		return attachmentRepository.findAll();
	}

}
