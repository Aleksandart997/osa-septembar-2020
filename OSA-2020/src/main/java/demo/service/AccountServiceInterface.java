package demo.service;

import java.util.List;

import demo.entity.Account;

public interface AccountServiceInterface {

	Account getOne(Long id);
	Account findByUsername(String username);
	List<Account> findAll();
	Account save(Account account);
	void delete(Account account);
}
