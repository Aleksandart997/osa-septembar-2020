package demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.entity.Message;
import demo.repository.MessageRepository;

@Service
public class MessageService {

	
	@Autowired
	MessageRepository messageRepository;
	
	
	public Message getOne(Long id) {
		return messageRepository.getOne(id);
	}
	
	public Message save(Message message) {
		return messageRepository.save(message);
	}

	public void delete(Message message) {
		messageRepository.delete(message);
	}
	
	public List<Message> findAll() {
		return messageRepository.findAll();
	}

}
