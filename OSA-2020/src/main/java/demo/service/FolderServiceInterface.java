package demo.service;

import java.util.List;

import demo.entity.Account;
import demo.entity.Folder;

public interface FolderServiceInterface {
	
	Folder getOne(Long id);
	List<Folder> findAll();
	Folder findByName(String name);
	Folder findByAccount(Account account);
	Folder save(Folder folder);
	void delete(Folder folder);

}
