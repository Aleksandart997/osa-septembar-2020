package demo.service;

import java.util.List;

import demo.entity.Contact;

public interface ContactServiceInterface {

	
	Contact getOne(Long id);
	List<Contact> findAll();
	List<Contact> findByFirstname(String firstname);
	List<Contact> findByLastName(String lastname);
	Contact findByDisplayname(String displayname);
	Contact findByEmail(String email);
	Contact save(Contact contact);
	void delete(Contact contact);
	
}
