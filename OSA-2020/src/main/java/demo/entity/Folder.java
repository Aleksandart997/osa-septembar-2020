package demo.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import demo.dto.FolderDTO;


@Entity
@Table(name="folders")
public class Folder implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name="name", unique=false, nullable=false)
	private String name;
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="parent", nullable=true)
	private Folder parent;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="account", referencedColumnName = "id", nullable=false)
	private Account account;
	@OneToMany(cascade={CascadeType.REMOVE}, fetch=FetchType.EAGER, mappedBy="folder")
	private Set<Message> messages = new HashSet<Message>();
	@OneToMany(cascade={CascadeType.REMOVE, CascadeType.PERSIST}, fetch=FetchType.LAZY, mappedBy="parent")
	private Set<Folder> folders = new HashSet<Folder>();
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="folder")
	private Set<Rule> rules = new HashSet<Rule>();
	

	
	public Folder(Long id, String name, Folder parent, Account account, Set<Message> messages, Set<Folder> folders,
			Set<Rule> rules) {
		super();
		this.id = id;
		this.name = name;
		this.parent = parent;
		this.account = account;
		this.messages = messages;
		this.folders = folders;
		this.rules = rules;
	}

	public Set<Folder> getFolders() {
		return folders;
	}

	public void setFolders(Set<Folder> folders) {
		this.folders = folders;
	}

	public Set<Rule> getRules() {
		return rules;
	}

	public void setRules(Set<Rule> rules) {
		this.rules = rules;
	}

	public Folder() {
		super();
	}
	

	
	public Folder(FolderDTO f) {
		this.id = f.getId();
		this.name = f.getName();
		//DODATI MESSAGES I RULES
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Folder getParent() {
		return parent;
	}

	public void setParent(Folder parent) {
		this.parent = parent;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	

	public Set<Message> getMessages() {
		return messages;
	}

	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}

	@Override
	public String toString() {
		return "Folder [id=" + id + ", name=" + name + ", parent=" + parent + ", account="
				+ account + ", messages=" + messages+"]";
	}
	
	
	
	

}
