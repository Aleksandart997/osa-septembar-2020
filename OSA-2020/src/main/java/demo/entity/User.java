package demo.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import demo.dto.AccountDTO;
import demo.dto.ContactDTO;
import demo.dto.TagDTO;
import demo.dto.UserDTO;

@Entity
@Table(name = "users")
public class User implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name="username", unique=true, nullable=false)
	private String username;
	@Column(name="password", unique=false, nullable=false)
	private String password;
	@Column(name="first_name", unique=false, nullable=false)
	private String firstname;
	@Column(name="last_name", unique=false, nullable=false)
	private String lastname;
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="user")
	private Set<Contact> contacts = new HashSet<Contact>();
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="user")
	private Set<Account> accounts = new HashSet<Account>();
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="user")
	private Set<Tag> tags = new HashSet<Tag>();
	
	
	public User() {
		super();
	}

	
	
	public User(Long id, String username, String password, String firstname, String lastname,
			Set<Contact> contacts, Set<Account> accounts, Set<Tag> tags) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.contacts = contacts;
		this.accounts = accounts;
		this.tags = tags;
	}

	public User(UserDTO userDTO) {
		super();
		this.id = userDTO.getId();
		this.username = userDTO.getUsername();
		this.password = userDTO.getPassword();
		this.firstname = userDTO.getFirstname();
		this.lastname = userDTO.getLastname();
		Set<Contact> contacts = new HashSet<>();
		for(ContactDTO c : userDTO.getContacts())
			contacts.add(new Contact(c));
		this.contacts = contacts;
		Set<Account> accounts = new HashSet<>();
		for(AccountDTO c : userDTO.getAccounts())
			accounts.add(new Account(c));
		this.accounts = accounts;
		Set<Tag> tags = new HashSet<>();
		for(TagDTO c : userDTO.getTags())
			tags.add(new Tag(c));
		this.tags = tags;
	}


	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public Set<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", contacts=" + contacts + ", accounts=" + accounts + ", tags="+ tags +"]";
	}
	
}
