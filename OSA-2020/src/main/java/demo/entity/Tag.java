package demo.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import demo.dto.TagDTO;

@Entity
@Table(name="tags")
public class Tag implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name="name", unique=false, nullable=false)
	private String name;
	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "tag")
	private Set<Message> messages = new HashSet<Message>();
	@ManyToOne
	@JoinColumn(name="user", nullable=false)
	private User user;

	public Tag(Long id, String name, List<Message> message, User user) {
		super();
		this.id = id;
		this.name = name;
		//this.message = message;
		this.user = user;
	}

	public Tag(TagDTO tagDTO) {
		super();
		this.id = tagDTO.getId();
		this.name = tagDTO.getName();
	}
	
	public Tag() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Message> getMessage() {
		return messages;
	}

	public void setMessage(Set<Message> messages) {
		this.messages = messages;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Tag [id=" + id + ", name=" + name + ", message=" + messages + ", user=" + user + "]";
	}
	
	

}
