package demo.entity;

import java.io.Serializable;

import javax.persistence.*;

import demo.dto.PhotoDTO;
@Entity
@Table(name="photos")
public class Photo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name="path", unique=false, nullable=false)
	private String path;
	@ManyToOne
	@JoinColumn(name = "contact", referencedColumnName = "id", nullable = false)
	private Contact contact;
	
	
	public Photo(Long id, String path, Contact contact) {
		super();
		this.id = id;
		this.path = path;
		this.contact = contact;
	}
	public Photo() {
		super();
	}
	public Photo(PhotoDTO photo) {
		this.id = photo.getId();
		this.path = photo.getPath();
		Contact c = new Contact(photo.getContact());
		this.contact = c;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	@Override
	public String toString() {
		return "Photo [id=" + id + ", path=" + path + "]";
	}
	
	
}
