package demo.entity;

import java.io.Serializable;
import java.util.Base64;

import javax.persistence.*;

import demo.dto.AttachmentDTO;


@Entity
@Table(name="attachments")
public class Attachment implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Lob
	@Basic(fetch=FetchType.LAZY)
	private Base64 data;
	@Column(name="type", unique=false, nullable=false)
	private String type;
	@Column(name="name", unique=false, nullable=false)
	private String name;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="message", referencedColumnName="id", nullable=true)
	private Message message;

	public Attachment (Long id, Base64 data, String type, String name, Message message ){
		super();
		this.id = id;
		this.data = data;
		this.type = type;
		this.message = message;	
	}
	
	public Attachment() {
		super();
	}
	
	public Attachment( AttachmentDTO attachment) {
		super();
		this.id = attachment.getId();
		this.data = attachment.getData();
		this.type = attachment.getType();
		this.name = attachment.getName();
		Message msg = new Message(attachment.getMessage());
		this.message = msg;
		
		
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Base64 getData() {
		return data;
	}

	public void setData(Base64 data) {
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Attachment [id=" + id + ", data=" + data + ", type=" + type + ", name=" + name + ", message=" + message
				+ "]";
	}
	
	
	

}
