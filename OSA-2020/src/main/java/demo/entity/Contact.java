package demo.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import demo.dto.ContactDTO;

@Entity
@Table(name="contacts")
public class Contact implements Serializable{

	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name="firstname", unique=false, nullable=false)
	private String firstname;
	@Column(name="lastname", unique=false, nullable=false)
	private String lastname;
	@Column(name="displayname", unique=false, nullable=false)
	private String displayname;
	@Column(name="email", unique=false, nullable=false)
	private String email;
	@Column(name="note", unique=false, nullable=true)
	private String note;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user", referencedColumnName="id", nullable=false)	
	private User user;
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "contact")
	private Set<Photo> photos = new HashSet<Photo>();
	
	
	public Contact(Long id, String firstname, String lastname, String displayname, String email, String note,
			Set<Photo> photo, User user) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.displayname = displayname;
		this.email = email;
		this.note = note;
		this.photos = photo;
		this.user = user;
	}
	public Contact() {
		super();
	}
	public Contact(ContactDTO c) {
		this.id = c.getId();
		this.firstname = c.getFirstname();
		this.lastname = c.getLastname();
		this.displayname = c.getDisplayname();
		this.email = c.getEmail();
		this.note = c.getNote();
		User user = new User(c.getUser());
		this.user = user;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Set<Photo> getPhoto() {
		return photos;
	}
	public void setPhoto(Set<Photo> photos) {
		this.photos = photos;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Contact [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", displayname="
				+ displayname + ", email=" + email + ", note=" + note + ", photo=" + photos + ", user=" + user + "]";
	}
}
