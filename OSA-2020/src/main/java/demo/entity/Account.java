package demo.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import demo.dto.AccountDTO;

@Entity
@Table(name="accounts")
public class Account implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name="smtp_address", unique=false, nullable=false)
	private String smtpAddress;
	@Column(name="smtp_port", unique=false, nullable=false)
	private Integer smtpPort;
	@Column(name="in_server_type", unique=false, nullable=false)
	private Short inServerType;
	@Column(name="in_server_address", unique=false, nullable=false)
	private String inServerAddress;
	@Column(name="in_server_port", unique=false, nullable=false)
	private Integer inServerPort;
	@Column(name="username", unique=false, nullable=false)
	private String username;
	@Column(name="password", unique=false, nullable=false)
	private String password;
	@Column(name="display_name", unique=false, nullable=false)
	private String displayname;
	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="user", referencedColumnName="id",  nullable=false)
	private User user;
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="account")
	private List<Folder> folders = new ArrayList<Folder>();
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="account")
	private List<Message> messages = new ArrayList<Message>();

	public Account(Long id, String smtpAddress, Integer smtpPort, Short inServerType, String inServerAddress,
			Integer inServerPort, String username, String password, String displayname, User user) {
		super();
		this.id = id;
		this.smtpAddress = smtpAddress;
		this.smtpPort = smtpPort;
		this.inServerType = inServerType;
		this.inServerAddress = inServerAddress;
		this.inServerPort = inServerPort;
		this.username = username;
		this.password = password;
		this.displayname = displayname;
		this.user = user;
	}

	
	public Account() {
		super();
	}


	public Account(AccountDTO a) {
		this.id = a.getId();
		this.smtpAddress = a.getSmtpAddress();
		this.smtpPort = a.getSmtpPort();
		this.inServerType = a.getInServerType();
		this.inServerAddress = a.getInServerAddress();
		this.inServerPort = a.getInServerPort();
		this.username = a.getUsername();
		this.password = a.getPassword();
		this.displayname = a.getDisplayname();
		User user = new User(a.getUser());
		this.user = user;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSmtpAddress() {
		return smtpAddress;
	}


	public List<Folder> getFolders() {
		return folders;
	}

	public void setFolders(List<Folder> folders) {
		this.folders = folders;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public void setSmtpAddress(String smtpAddress) {
		this.smtpAddress = smtpAddress;
	}

	public Integer getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}

	public Short getInServerType() {
		return inServerType;
	}

	public void setInServerType(Short inServerType) {
		this.inServerType = inServerType;
	}

	public String getInServerAddress() {
		return inServerAddress;
	}

	public void setInServerAddress(String inServerAddress) {
		this.inServerAddress = inServerAddress;
	}

	public Integer getInServerPort() {
		return inServerPort;
	}

	public void setInServerPort(Integer inServerPort) {
		this.inServerPort = inServerPort;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String toString() {
		return "Account [id=" + id + ", smtpAddress=" + smtpAddress + ", smtpPort=" + smtpPort + ", inServerType="
				+ inServerType + ", inServerAddress=" + inServerAddress + ", inServerPort=" + inServerPort
				+ ", username=" + username + ", password=" + password + ", displayname=" + displayname + ", user="
				+ user + ", folders=" + folders + ", messages=" + messages + "]";
	}
}
