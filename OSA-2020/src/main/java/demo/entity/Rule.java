package demo.entity;

import javax.persistence.*;

import demo.dto.RuleDTO;

import java.io.Serializable;

import javax.persistence.Column;

@Entity
@Table(name="rules")
public class Rule implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public enum RuleCondition {
		TO,
		FROM,
		CC,
		SUBJECT
	}
	
	public enum RuleOperation {
		MOVE,
		COPY,
		DELETE
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Enumerated
	@Column(name="r_condition", unique=false, nullable=true)
	private RuleCondition condition;
	@Column(name="r_value", unique=false, nullable=true)
	private String value;
	@Enumerated
	@Column(name="r_operation", unique=false, nullable=true)
	private RuleOperation operation;
	@ManyToOne
	@JoinColumn(name="folder", referencedColumnName="id", nullable=true)
	private Folder folder;
	
	public Rule() {
	  }
	  
	public Rule(RuleCondition condition, String value, RuleOperation operation, Folder folder) {
		super();
		this.condition = condition;
		this.value = value;
		this.operation = operation;
		this.folder = folder;
	  }
	
	public Rule(RuleDTO ruleDTO) {
		super();
		this.id = ruleDTO.getId();
		this.condition = ruleDTO.getRuleCondition();
		this.value = ruleDTO.getValue();
		this.operation = ruleDTO.getRuleOperation();		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RuleCondition getCondition() {
		return condition;
	}

	public void setCondition(RuleCondition condition) {
		this.condition = condition;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public RuleOperation getOperation() {
		return operation;
	}

	public void setOperation(RuleOperation operation) {
		this.operation = operation;
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	@Override
	public String toString() {
		return "Rule [id=" + id + ", condition=" + condition + ", value=" + value + ", operation=" + operation
				+ ", folder=" + folder + "]";
	}
	
	
	
	
	
	

}
