package demo.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import demo.dto.MessageDTO;


@Entity
@Table(name="messages")
public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	@Column(name="subject", unique=false, nullable=false)
	private String subject;
	@Column(name="content", unique=false, nullable=false)
	private String content;
	@Column(name="date_and_time", unique=false, nullable=false)
	private LocalDateTime date;
	@ManyToOne
	@JoinColumn(name="account", referencedColumnName="id", nullable=false)
	private Account account;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="folder", referencedColumnName="id", nullable=false)
	private Folder folder;
	@ManyToMany
	@JoinTable(name="message_tag", joinColumns= {	@JoinColumn(name="message_id")	}, inverseJoinColumns = {	@JoinColumn(name="tag_id")	})
	private Set<Tag> tag = new HashSet<Tag>();
	@Column(name="m_from", unique=false, nullable=false)
	private String from;
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="message")
	private Set<Attachment> attachments = new HashSet<Attachment>(); 
	@Column(name="contacts_TO", unique=false, nullable=true)
	private String contactsTo ;	
	@Column(name="contacts_CC", unique=false, nullable=true)
	private String contactsCC ;
	@Column(name="contacts_BCC", unique=false, nullable=true)
	private String contactsBCC ;
	@Column(name="unread", unique=false, nullable=false)
	private boolean unread;
	
	public Message() {}

	public Message(Long id, String title, String message, LocalDateTime date, Account account, Folder folder,
			/*Set<Tag> tag,*/ String from, /*Set<Attachment> attachments,*/ String contactsTo, String contactsCC,
			String contactsBCC/*, Set<Tag> tags*/, boolean unread) {
		super();
		this.id = id;
		this.subject = title;
		this.content = message;
		this.date = date;
		this.account = account;
		this.folder = folder;
		//this.tag = tag;
		this.from = from;
		//this.attachments = attachments;
		this.contactsTo = contactsTo;
		this.contactsCC = contactsCC;
		this.contactsBCC = contactsBCC;
		//this.tags = tags;
		this.unread = unread;
	}
	
	
	public Message (MessageDTO message2) {
		this.id = message2.getId();
		this.subject = message2.getTitle();
		this.content = message2.getMessage();
		this.date = message2.getDate();
		this.from = message2.getFrom();
		this.contactsTo = message2.getTo();
		this.contactsCC = message2.getCc();
		this.contactsBCC = message2.getBcc();
		
	}

	
//	public void addTag(Tag tag) {
//		tag.getMessage().add(this);
//		getTags().add(tag);
//	}
//	
//	public void removeTag(Tag tag) {
//		tag.getMessage().remove(this);
//		getTags().remove(tag);
//	}
	
	public void addAttachment(Attachment attachment) {
		if(attachment.getMessage() != null)
			attachment.getMessage().removeAttachment(attachment);
		attachment.setMessage(this);
		getAttachments().add(attachment);
	}
	
	
	private void removeAttachment(Attachment attachment) {
		attachment.setMessage(null);
		getAttachments().remove(attachment);
	}


	public boolean isUnread() {
		return unread;
	  }
	
	public void setUnread(boolean unread) {
		this.unread = unread;
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return subject;
	}

	public void setTitle(String title) {
		this.subject = title;
	}

	public String getMessage() {
		return content;
	}

	public void setMessage(String message) {
		this.content = message;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}


	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public Set<Tag> getTag() {
		return tag;
	}

	public void setTag(Set<Tag> tag) {
		this.tag = tag;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public Set<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<Attachment> attachments) {
		this.attachments = attachments;
	}

	public String getContactsTo() {
		return contactsTo;
	}

	public void setContactsTo(String contactsTo) {
		this.contactsTo = contactsTo;
	}

	public String getContactsCC() {
		return contactsCC;
	}

	public void setContactsCC(String contactsCC) {
		this.contactsCC = contactsCC;
	}

	public String getContactsBCC() {
		return contactsBCC;
	}

	public void setContactsBCC(String contactsBCC) {
		this.contactsBCC = contactsBCC;
	}


	@Override
	public String toString() {
		return "Message [id=" + id + ", title=" + subject + ", message=" + content + ", date=" + date + ", account=" + account + ", folder=" + folder + ", tag=" + tag + ", from=" + from
				+ ", attachments=" + attachments + ", contactsTo=" + contactsTo + ", contactsCC=" + contactsCC
				+ ", contactsBCC=" + contactsBCC + ", unread=" + unread + "]";
	}
	
	
	

}
