package demo.dto;

import java.util.Base64;

import demo.entity.Attachment;
import demo.entity.Message;

public class AttachmentDTO {
	
	private Long id;
	private Base64 data;
	private String type;
	private String name;
	private MessageDTO message;
	
	public AttachmentDTO() {
		super();
	}
	
	public AttachmentDTO (Long id, Base64 data, String type, String name, MessageDTO message) {
		this.id = id;
		this.data = data;
		this.type = type;
		this.name = name;
		this.message = message;
	}
	
	public AttachmentDTO (Attachment attachment) {
		super();
		this.id = attachment.getId();
		this.data = attachment.getData();
		this.type = attachment.getType();
		this.name = attachment.getName();
		MessageDTO msg = new MessageDTO(attachment.getMessage());
			this.message = msg;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Base64 getData() {
		return data;
	}

	public void setData(Base64 data) {
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MessageDTO getMessage() {
		return message;
	}

	public void setMessage(MessageDTO message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "AttachmentDTO [id=" + id + ", data=" + data + ", type=" + type + ", name=" + name + ", message="
				+ message + "]";
	}
	
	
	

}
