
package demo.dto;

import java.util.HashSet;
import java.util.Set;

import demo.entity.Account;
import demo.entity.Contact;
import demo.entity.Tag;
import demo.entity.User;

public class UserDTO {

	
	private Long id;
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	
	private Set<ContactDTO> contacts = new HashSet<ContactDTO>();
	private Set<AccountDTO> accounts= new HashSet<AccountDTO>();
	private Set<TagDTO> tags = new HashSet<TagDTO>();
	
	public UserDTO() {
		super();
	}
	
	
	
	public UserDTO(Long id, String username, String password, String firstname, String lastname,
			Set<ContactDTO> contacts, Set<AccountDTO> accounts, Set<TagDTO> tags) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.contacts = contacts;
		this.accounts = accounts;
		this.tags = tags;
	}



	public UserDTO(User user) {
		super();
		this.id = user.getId();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.firstname = user.getFirstname();
		this.lastname = user.getLastname();
		Set<ContactDTO> contactsDTO = new HashSet<ContactDTO>();
		if(user.getContacts() != null) {
			for(Contact c : user.getContacts())
				contactsDTO.add(new ContactDTO(c));
		}
		this.contacts = contactsDTO;
		Set<AccountDTO> accountsDTO = new HashSet<AccountDTO>();
		if (user.getAccounts() != null) {
	    	for (Account a : user.getAccounts())
	    		accountsDTO.add(new AccountDTO(a));
		}
		this.accounts = accountsDTO;
		Set<TagDTO> tagsDTO = new HashSet<TagDTO>();
		if(user.getTags() != null)
			for(Tag t : user.getTags())
				tagsDTO.add(new TagDTO(t));
		this.tags = tagsDTO;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}



	public Set<ContactDTO> getContacts() {
		return contacts;
	}



	public void setContacts(Set<ContactDTO> contacts) {
		this.contacts = contacts;
	}



	public Set<AccountDTO> getAccounts() {
		return accounts;
	}



	public void setAccounts(Set<AccountDTO> accounts) {
		this.accounts = accounts;
	}



	public Set<TagDTO> getTags() {
		return tags;
	}



	public void setTags(Set<TagDTO> tags) {
		this.tags = tags;
	}
}

