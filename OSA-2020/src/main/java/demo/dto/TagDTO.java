package demo.dto;

import java.util.HashSet;
import java.util.Set;

import demo.entity.Message;
import demo.entity.Tag;
import demo.entity.User;

public class TagDTO {
	
	private Long id;
    private String name;
    private Set<MessageDTO> message;
    private UserDTO user;
    public TagDTO() {
		super();
	}
    
    public TagDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

	public TagDTO(Tag t) {
		super();
		this.id = t.getId();
		this.name = t.getName();
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<MessageDTO> getMessage() {
		return message;
	}

	public void setMessage(Set<MessageDTO> message) {
		this.message = message;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "TagDTO [id=" + id + ", name=" + name + "]";
	}
	
	
    
    

}
