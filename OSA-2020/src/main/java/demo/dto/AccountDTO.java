package demo.dto;

import java.util.HashSet;
import java.util.Set;

import demo.entity.Account;
import demo.entity.Folder;
import demo.entity.Message;


public class AccountDTO {

	private Long id;
	private String smtpAddress;
	private Integer smtpPort;
	private Short inServerType;
	private String inServerAddress;
	private Integer inServerPort;
	private String username;
	private String password;
	private String displayname;
	private UserDTO user;
	private Set<FolderDTO> folders = new HashSet<FolderDTO>();
	private Set<MessageDTO> messages = new HashSet<MessageDTO>();
	
	
	public AccountDTO() {
		super();
	}
	
	
	public AccountDTO(Long id, String smtpAddress, Integer smtpPort, Short inServerType, String inServerAddress,
			Integer inServerPort, String username, String password, String displayname, UserDTO user,
			Set<FolderDTO> folders, Set<MessageDTO> messages) {
		super();
		this.id = id;
		this.smtpAddress = smtpAddress;
		this.smtpPort = smtpPort;
		this.inServerType = inServerType;
		this.inServerAddress = inServerAddress;
		this.inServerPort = inServerPort;
		this.username = username;
		this.password = password;
		this.displayname = displayname;
		this.user = user;
		this.folders = folders;
		this.messages = messages;
	}

	public AccountDTO(Account account) {
		super();
		this.id = account.getId();
		this.smtpAddress = account.getSmtpAddress();
		this.smtpPort = account.getSmtpPort();
		this.inServerType = account.getInServerType();
		this.inServerAddress = account.getInServerAddress();
		this.inServerPort = account.getInServerPort();
		this.username = account.getUsername();
		this.password = account.getPassword();
		this.displayname = account.getDisplayname();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSmtpAddress() {
		return smtpAddress;
	}
	public void setSmtpAddress(String smtpAddress) {
		this.smtpAddress = smtpAddress;
	}
	public Integer getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}
	public Short getInServerType() {
		return inServerType;
	}
	public void setInServerType(Short inServerType) {
		this.inServerType = inServerType;
	}
	public String getInServerAddress() {
		return inServerAddress;
	}
	public void setInServerAddress(String inServerAddress) {
		this.inServerAddress = inServerAddress;
	}
	public Integer getInServerPort() {
		return inServerPort;
	}
	public void setInServerPort(Integer inServerPort) {
		this.inServerPort = inServerPort;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public Set<FolderDTO> getFolders() {
		return folders;
	}
	public void setFolders(Set<FolderDTO> folders) {
		this.folders = folders;
	}
	public Set<MessageDTO> getMessages() {
		return messages;
	}
	public void setMessages(Set<MessageDTO> messages) {
		this.messages = messages;
	}
}
