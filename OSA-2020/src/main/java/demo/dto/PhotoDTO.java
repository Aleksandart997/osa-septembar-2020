package demo.dto;

import demo.entity.Photo;

public class PhotoDTO {

	private Long id;
	private String path;
	private ContactDTO contact;
	
	public PhotoDTO() {
		super();
	}
	
	public PhotoDTO(Long id, String path, ContactDTO contact) {
		this.id = id;
		this.path = path;
		this.contact = contact;
	}
	
	public PhotoDTO(Photo photo) {
		this.id = photo.getId();
		this.path = photo.getPath();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public ContactDTO getContact() {
		return contact;
	}

	public void setContact(ContactDTO contact) {
		this.contact = contact;
	}

	@Override
	public String toString() {
		return "PhotoDTO [id=" + id + ", path=" + path + "]";
	}

	
}
