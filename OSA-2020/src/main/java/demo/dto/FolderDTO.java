package demo.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import demo.entity.Folder;
import demo.entity.Message;
import demo.entity.Rule;


public class FolderDTO {
	
	private Long id;
    private String name;
    private FolderDTO parent;
    private AccountDTO account;
    private Set<FolderDTO> folders = new HashSet<FolderDTO>();
    private Set<MessageDTO> messages = new HashSet<MessageDTO>();
    private Set<RuleDTO> rules = new HashSet<RuleDTO>();

    
    public FolderDTO() {
		super();
	}
	
	public FolderDTO(Long id, String name, Set<MessageDTO> messages, Set<RuleDTO> rules, FolderDTO parent,
			AccountDTO account, Set<FolderDTO> folders) {
		this.id = id;
		this.name = name;
		this.messages = messages;
		this.rules = rules;
		this.parent = parent;
		this.account = account;
		this.folders = folders;
	}
	
	public FolderDTO(Folder folder) {
		Folder parentFolder = folder.getParent();
    	if (parentFolder != null) {
    		this.parent= new FolderDTO();
        	this.parent.setId(parentFolder.getId());
        	this.parent.setName(parentFolder.getName());
        	this.parent.setRules(new HashSet<RuleDTO>());
        	this.parent.setFolders(new HashSet<FolderDTO>());
    	}
    	
    	for (Folder subFolder : folder.getFolders()) {
    		FolderDTO subFolderDTO = new FolderDTO();
    		subFolderDTO.setId(subFolder.getId());
    		subFolderDTO.setName(subFolder.getName());
    		subFolderDTO.setRules(new HashSet<RuleDTO>());
    		subFolderDTO.setFolders(new HashSet<FolderDTO>());
    		folders.add(subFolderDTO);
    	}
    	
    	for (Rule rule : folder.getRules()) {
    		rules.add(new RuleDTO(rule));
    	}
    	
    	for (Message message : folder.getMessages()) {
    		messages.add(new MessageDTO(message));
    	}
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public FolderDTO getParent() {
		return parent;
	}

	public void setParent(FolderDTO parent) {
		this.parent = parent;
	}

	public AccountDTO getAccount() {
		return account;
	}

	public void setAccount(AccountDTO account) {
		this.account = account;
	}

	public Set<FolderDTO> getFolders() {
		return folders;
	}

	public void setFolders(Set<FolderDTO> folders) {
		this.folders = folders;
	}

	public Set<MessageDTO> getMessages() {
		return messages;
	}

	public void setMessages(Set<MessageDTO> messages) {
		this.messages = messages;
	}

	public Set<RuleDTO> getRules() {
		return rules;
	}

	public void setRules(Set<RuleDTO> rules) {
		this.rules = rules;
	}
	

	@Override
	public String toString() {
		return "FolderDTO [id=" + id + ", name=" + name + ", parent=" + parent + ", account="
				+ account + ", folders=" + folders + ", messages=" + messages + ", rules=" + rules + "]";
	}

	
	
	
	
	
}
