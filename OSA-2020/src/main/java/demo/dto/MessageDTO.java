package demo.dto;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import demo.entity.Account;
import demo.entity.Folder;
import demo.entity.Message;

public class MessageDTO {
	
	private Long id;
    private String from;
    private String to;
    private String title;
    private String message;
    private LocalDateTime date;
    private PhotoDTO photo;
    private String cc;
    private String bcc;
    private FolderDTO folder;
    private Set<TagDTO> tags;
    private Set<AttachmentDTO> attachments;
    private AccountDTO account;
    private boolean unread;

    
    public MessageDTO(Long id, String from, String to, String title, String message, 
    		LocalDateTime date, PhotoDTO photo, String cc, String bcc, FolderDTO folder, 
			Set<TagDTO> tags, Set<AttachmentDTO> attachments, AccountDTO account, boolean unread){
        this.id = id;
        this.from = from;
        this.to = to;
        this.title = title;
        this.message = message;
        this.date = date;
        this.photo = photo;
        this.cc = cc;
        this.bcc = bcc;
        this.folder = folder;
        this.tags = tags;
        this.attachments = attachments;
        this.account = account;
        this.unread = unread;
    }
    
	public MessageDTO() {
		super();
	}
	

	public MessageDTO(Message message2) {
//		this.id = message2.getId();
//        this.title = message2.getTitle();
//        this.from = message2.getFrom();
//        this.message = message2.getMessage();
//        this.date = message2.getDate();
//        PhotoDTO photo = new PhotoDTO(message2.getPhoto());
//		this.photo = photo;
//		Set<AccountDTO> accoungDTO = new HashSet<AccountDTO>();
//		if(message2.getAccount() != null)
//			for(Account a : message2.getAccount())
//				AccountDTO.add(new AccountDTO(a));
//		this.folders = accountDTO;
//        this.folder = message2.getFolder();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public PhotoDTO getPhoto() {
		return photo;
	}

	public void setPhoto(PhotoDTO photo) {
		this.photo = photo;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public FolderDTO getFolder() {
		return folder;
	}

	public void setFolder(FolderDTO folder) {
		this.folder = folder;
	}

	public Set<TagDTO> getTags() {
		return tags;
	}

	public void setTags(Set<TagDTO> tags) {
		this.tags = tags;
	}

	public Set<AttachmentDTO> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<AttachmentDTO> attachments) {
		this.attachments = attachments;
	}

	public AccountDTO getAccount() {
		return account;
	}

	public void setAccount(AccountDTO account) {
		this.account = account;
	}

	public boolean isUnread() {
		return unread;
	}

	public void setUnread(boolean unread) {
		this.unread = unread;
	}

	@Override
	public String toString() {
		return "MessageDTO [id=" + id + ", from=" + from + ", to=" + to + ", title=" + title + ", message=" + message
				+ ", date=" + date + ", avatar=" + photo + ", cc=" + cc + ", bcc=" + bcc + ", folder=" + folder
				+ ", tags=" + tags + ", attachments=" + attachments + ", account=" + account + ", unread=" + unread
				+ "]";
	}
	
	
	
	
}
