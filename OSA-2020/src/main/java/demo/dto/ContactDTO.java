package demo.dto;

import demo.entity.Contact;

import java.util.HashSet;
import java.util.Set;

import demo.entity.Photo;

public class ContactDTO {

	private Long id;
	private String firstname;
	private String lastname;
	private String displayname;
	private String email;
	private String note;
	private Set<PhotoDTO> photos = new HashSet<PhotoDTO>();
	private UserDTO user;
	
	public ContactDTO() {
		super();
	}
	public ContactDTO(Long id, String firstname, String lastname, String displayname, String email, String note,
			Set<PhotoDTO> photo, UserDTO user) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.displayname = displayname;
		this.email = email;
		this.note = note;
		this.photos = photo;
		this.user = user;
	}
	
	public ContactDTO(Contact contact) {
		super();
		this.id = contact.getId();
		this.firstname = contact.getFirstname();
		this.lastname = contact.getLastname();
		this.displayname = contact.getDisplayname();
		this.email = contact.getEmail();
		this.note = contact.getNote();
		for (Photo photo : contact.getPhoto()) {
			photos.add(new PhotoDTO(photo));
		}
		}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Set<PhotoDTO> getPhoto() {
		return photos;
	}
	public void setPhoto(Set<PhotoDTO> photo) {
		this.photos = photo;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "ContactDTO [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", displayname="
				+ displayname + ", email=" + email + ", note=" + note + ", photo=" + photos + ", user=" + user + "]";
	}
	
	
}
