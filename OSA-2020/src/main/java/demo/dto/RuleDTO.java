package demo.dto;

import demo.entity.Rule;

public class RuleDTO {
	
	private Long id;
    private Rule.RuleCondition condition;
    private Rule.RuleOperation operation;
    private String value;
    private FolderDTO folderdto;

    
    public RuleDTO() {
		super();
	}
	
	public RuleDTO(Long id, Rule.RuleCondition condition, Rule.RuleOperation operation, String value, FolderDTO folderdto) {
        this.id = id;
        this.condition = condition;
        this.operation = operation;
        this.value = value;
        this.folderdto = folderdto;
    }
	
	public RuleDTO(Rule rule) {
		super();
		this.id = rule.getId();
		this.condition = rule.getCondition();
		this.operation = rule.getOperation();
		this.value = rule.getValue();
		this.folderdto = new FolderDTO(rule.getFolder());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Rule.RuleCondition getRuleCondition() {
		return condition;
	}

	public void setRuleCondition(Rule.RuleCondition condition) {
		this.condition = condition;
	}

	public Rule.RuleOperation getRuleOperation() {
		return operation;
	}

	public void setRuleOperation(Rule.RuleOperation operation) {
		this.operation = operation;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	

	public FolderDTO getFolderdto() {
		return folderdto;
	}

	public void setFolderdto(FolderDTO folderdto) {
		this.folderdto = folderdto;
	}

	@Override
	public String toString() {
		return "RuleDTO [id=" + id + ", condition=" + condition + ", operation=" + operation + ", value=" + value + "]";
	}
	
	
}
