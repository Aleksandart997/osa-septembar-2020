package demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.entity.Account;
import demo.entity.Folder;

public interface FolderRepository extends JpaRepository<Folder, Long> {
	
	Folder findByName(String name);
	
	Folder findByAccount(Account account);
	

}
