package demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import demo.entity.Attachment;


public interface AttachmentRepository extends JpaRepository<Attachment, Long> {

}
