package demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.entity.Rule;

public interface RuleRepository extends JpaRepository<Rule, Long>{

}
