package demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.entity.Contact;
import demo.entity.Photo;

public interface PhotoRepository extends JpaRepository<Photo, Long>{

	List<Photo> findByContact(Contact contact);

	
}
