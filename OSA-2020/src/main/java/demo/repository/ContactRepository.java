package demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.entity.Contact;

public interface ContactRepository extends JpaRepository<Contact, Long>{

	//Contact findByUsername(String username);

	Contact findByDisplayname(String username);

	List<Contact> findByFirstname(String firstname);

	List<Contact> findByLastname(String lastname);

	Contact findByEmail(String email);

}
