package demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.entity.User;


public interface UserRepository extends JpaRepository<User, Long>{

	User findByUsername(String username);

	User findByUsernameAndPassword(String username, String password);

	List<User> findAllByLastname(String lastname);

	List<User> findAllByFirstname(String firstname);

}

