package demo.viewcontroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AccountViewController {


	@RequestMapping("/accounts")
	public String viewAccounts()
	{
		return "account";
	}
}
