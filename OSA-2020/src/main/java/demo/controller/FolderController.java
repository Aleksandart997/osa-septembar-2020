package demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.dto.AccountDTO;
import demo.dto.ContactDTO;
import demo.dto.FolderDTO;
import demo.dto.MessageDTO;
import demo.entity.Account;
import demo.entity.Contact;
import demo.entity.Folder;
import demo.entity.Message;
import demo.entity.User;
import demo.service.FolderService;

@RestController
@RequestMapping(value="api/folders")
public class FolderController {
	
	@Autowired
	private FolderService folderService;
	
	@GetMapping("/all")
	public ResponseEntity<List<FolderDTO>> findAll(){
		List<FolderDTO> foldersDTO = new ArrayList<FolderDTO>();
		List<Folder> folders = folderService.findAll();
		for(Folder f : folders)
			foldersDTO.add(new FolderDTO(f));
		return new ResponseEntity<List<FolderDTO>>(foldersDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<FolderDTO> getOne(@PathVariable Long id){
		Folder folder = folderService.getOne(id);
		if(folder == null) {
			System.out.println("Folder not found.");
			return new ResponseEntity<FolderDTO>(HttpStatus.NOT_FOUND);
		}
		FolderDTO folderDTO = new FolderDTO(folder);
		return new ResponseEntity<FolderDTO>(folderDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{name}")
	public ResponseEntity<FolderDTO> findByName(@PathVariable String name){
		Folder folder = folderService.findByName(name);
		if(folder == null) {
			System.out.println("Folder name not found.");
			return new ResponseEntity<FolderDTO>(HttpStatus.NOT_FOUND);
		}
		FolderDTO folderDTO = new FolderDTO(folder);
		return new ResponseEntity<FolderDTO>(folderDTO, HttpStatus.OK);
		
	}
	
	@GetMapping("/{?}")
	public ResponseEntity<FolderDTO> findByAccount(@PathVariable Account account){
		Folder folder = folderService.findByAccount(account);
		if(folder == null) {
			System.out.println("Account folder not found.");
			return new ResponseEntity<FolderDTO>(HttpStatus.NOT_FOUND);
		}
		FolderDTO folderDTO = new FolderDTO(folder);
		return new ResponseEntity<FolderDTO>(folderDTO, HttpStatus.OK);
		
	}
	
	@PostMapping(value="/save",consumes="application/json")
	public ResponseEntity<FolderDTO> save(@RequestBody FolderDTO folderDTO){
		Folder folder = new Folder(folderDTO);
		folder = folderService.save(folder);
		return new ResponseEntity<FolderDTO>(new FolderDTO(folder), HttpStatus.CREATED);	
	}
	
	
	@DeleteMapping("/{id}")	public ResponseEntity<Void> delete(@PathVariable Long id){
		Folder folder = folderService.getOne(id);
		if (folder != null){
			folderService.delete(folder);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
