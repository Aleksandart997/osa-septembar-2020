package demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.dto.UserDTO;
import demo.entity.User;
import demo.service.UserService;

@RestController
@RequestMapping(value="api/login")
public class LoginController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/login") //????
	public ResponseEntity<UserDTO> login(@RequestParam("username") String username, @RequestParam("password") String password){
		User user = userService.findByUsernameAndPassword(username, password);
		if(user == null) {
			System.out.println("Wrong credentials.");
			return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<UserDTO>(HttpStatus.OK);
	}
	
}
