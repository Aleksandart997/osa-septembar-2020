
package demo.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.dto.AccountDTO;
import demo.dto.ContactDTO;
import demo.dto.TagDTO;
import demo.dto.UserDTO;
import demo.entity.Account;
import demo.entity.Contact;
import demo.entity.Tag;
import demo.entity.User;
import demo.service.UserService;

@RestController
@RequestMapping(value="api/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	//MISLIM DA NAM TREBA LOGIN CONTROLLER
	/*@RequestMapping(value="/login") //????
	public ResponseEntity<UserDTO> login(@RequestParam("username") String username, @RequestParam("password") String password){
		User user = userService.findByUsernameAndPassword(username, password);
		if(user == null) {
			System.out.println("Wrong credentials.");
			return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<UserDTO>(HttpStatus.OK);
	}*/
	
	@GetMapping//@RequestMapping(value="/all", method = RequestMethod.GET) moze da se stavi i ovo
	public ResponseEntity<List<UserDTO>> findAll(){
		List<UserDTO> usersDTO = new ArrayList<UserDTO>();
		List<User> users = userService.findAll();
		for(User u : users)
			usersDTO.add(new UserDTO(u));
		return new ResponseEntity<List<UserDTO>>(usersDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<UserDTO> getOne(@PathVariable Long id){
		User user = userService.getOne(id);
		if(user == null) {
			System.out.println("User not found.");
			return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		}
		UserDTO userDTO = new UserDTO(user);
		return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{username}")
	public ResponseEntity<UserDTO> findByUsername(@PathVariable String username){
		User user = userService.findByUsername(username);
		if(user == null) {
			System.out.println("User not found.");
			return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		}
		UserDTO userDTO = new UserDTO(user);
		return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{lastname}")
	public ResponseEntity<List<UserDTO>> findByLastname(@PathVariable String lastname){
		List<User> users = userService.findByLastname(lastname);
		if(users == null) {
			System.out.println("Users not found.");
			return new ResponseEntity<List<UserDTO>>(HttpStatus.NOT_FOUND);
		}
		List<UserDTO> usersDTO = new ArrayList<UserDTO>();
		for(User u : users)
			usersDTO.add(new UserDTO(u));
		return new ResponseEntity<List<UserDTO>>(usersDTO, HttpStatus.OK);
	}
	
	@PostMapping(value="/save",consumes="application/json")
	public ResponseEntity<UserDTO> save(@RequestBody UserDTO userDTO){
		User user = new User(userDTO);
		user = userService.save(user);
		return new ResponseEntity<UserDTO>(new UserDTO(user), HttpStatus.CREATED);	
	}
	
	@PutMapping(consumes="application/json")
	public ResponseEntity<UserDTO> update(@RequestBody UserDTO userDTO){
		//a student must exist
		User user = userService.getOne(userDTO.getId()); 
		if (user == null) {
			return new ResponseEntity<UserDTO>(HttpStatus.BAD_REQUEST);
		}
		user.setUsername(userDTO.getUsername());
		user.setPassword(userDTO.getPassword());
		user.setFirstname(userDTO.getFirstname());
		user.setLastname(userDTO.getLastname());
		Set<Account> accounts = new HashSet<Account>();
		for(AccountDTO a : userDTO.getAccounts()) {
			Account ac = new Account(a);
			accounts.add(ac);
		}
		user.setAccounts(accounts);
		Set<Contact> contacts = new HashSet<Contact>();
		for(ContactDTO a : userDTO.getContacts()) {
			Contact ac = new Contact(a);
			contacts.add(ac);
		}
		user.setContacts(contacts);
		Set<Tag> tags = new HashSet<Tag>();
		for(TagDTO t : userDTO.getTags()) {
			Tag tag = new Tag(t);
			tags.add(tag);
		}
		user.setTags(tags);
		
		user = userService.save(user);
		return new ResponseEntity<>(new UserDTO(user), HttpStatus.OK);	
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
		User user = userService.getOne(id);
		if (user != null){
			userService.delete(user);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
}

