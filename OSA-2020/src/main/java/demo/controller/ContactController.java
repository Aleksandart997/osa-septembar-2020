package demo.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.dto.ContactDTO;
import demo.dto.PhotoDTO;
import demo.entity.Contact;
import demo.entity.Photo;
import demo.entity.User;
import demo.service.ContactService;


@RestController
@RequestMapping(value="api/contacts")
public class ContactController {

	@Autowired
	private ContactService contactService;
	
	@GetMapping("/all")
	public ResponseEntity<List<ContactDTO>> findAll(){
		List<ContactDTO> contactsDTO = new ArrayList<ContactDTO>();
		List<Contact> contacts = contactService.findAll();
		for(Contact a : contacts)
			contactsDTO.add(new ContactDTO(a));
		return new ResponseEntity<List<ContactDTO>>(contactsDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ContactDTO> getOne(@PathVariable Long id){
		Contact contact = contactService.getOne(id);
		if(contact == null) {
			System.out.println("Contact not found.");
			return new ResponseEntity<ContactDTO>(HttpStatus.NOT_FOUND);
		}
		ContactDTO contactDTO = new ContactDTO(contact);
		return new ResponseEntity<ContactDTO>(contactDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{firstname}")
	public ResponseEntity<List<ContactDTO>> findByFirstname(@PathVariable String firstname){
		List<ContactDTO> contactsDTO = new ArrayList<ContactDTO>();
		List<Contact> contacts = contactService.findByFirstname(firstname);
		for(Contact a : contacts)
			contactsDTO.add(new ContactDTO(a));
		return new ResponseEntity<List<ContactDTO>>(contactsDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{lastname}")
	public ResponseEntity<List<ContactDTO>> findByLastname(@PathVariable String lastname){
		List<ContactDTO> contactsDTO = new ArrayList<ContactDTO>();
		List<Contact> contacts = contactService.findByLastName(lastname);
		for(Contact a : contacts)
			contactsDTO.add(new ContactDTO(a));
		return new ResponseEntity<List<ContactDTO>>(contactsDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{displayname}")
	public ResponseEntity<ContactDTO> findByDisplayname(@PathVariable String displayname){
		Contact contact = contactService.findByDisplayname(displayname);
		if(contact == null) {
			System.out.println("Contact not found.");
			return new ResponseEntity<ContactDTO>(HttpStatus.NOT_FOUND);
		}
		ContactDTO contactDTO = new ContactDTO(contact);
		return new ResponseEntity<ContactDTO>(contactDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{email}")
	public ResponseEntity<ContactDTO> findByEmail(@PathVariable String email){
		Contact contact = contactService.findByEmail(email);
		if(contact == null) {
			System.out.println("Contact not found.");
			return new ResponseEntity<ContactDTO>(HttpStatus.NOT_FOUND);
		}
		ContactDTO contactDTO = new ContactDTO(contact);
		return new ResponseEntity<ContactDTO>(contactDTO, HttpStatus.OK);
	}
	
	
	@PostMapping(value="/save",consumes="application/json")
	public ResponseEntity<ContactDTO> save(@RequestBody ContactDTO contactDTO){
		Contact contact = new Contact(contactDTO);
		contact = contactService.save(contact);
		return new ResponseEntity<ContactDTO>(new ContactDTO(contact), HttpStatus.CREATED);	
	}
	
	
	
	@PutMapping(consumes="application/json")
	public ResponseEntity<ContactDTO> update(@RequestBody ContactDTO contactDTO){
		//a student must exist
		Contact contact = contactService.getOne(contactDTO.getId()); 
		if (contact == null) {
			return new ResponseEntity<ContactDTO>(HttpStatus.BAD_REQUEST);
		}
		contact.setFirstname(contactDTO.getFirstname());
		contact.setLastname(contactDTO.getLastname());
		contact.setDisplayname(contactDTO.getDisplayname());
		contact.setEmail(contactDTO.getEmail());
		contact.setNote(contactDTO.getNote());
		Set<Photo> photos = new HashSet<Photo>();
		for(PhotoDTO pdto : contactDTO.getPhoto()) {
			Photo p = new Photo(pdto);
			photos.add(p);
		}
		contact.setPhoto(photos);
		contact.setUser(new User(contactDTO.getUser()));
		
		
		contact = contactService.save(contact);
		return new ResponseEntity<ContactDTO>(new ContactDTO(contact), HttpStatus.OK);	
	}
	
	@DeleteMapping("/{id}")	public ResponseEntity<Void> delete(@PathVariable Long id){
		Contact contact = contactService.getOne(id);
		if (contact != null){
			contactService.delete(contact);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
