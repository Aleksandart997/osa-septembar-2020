package demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.dto.PhotoDTO;
import demo.entity.Contact;
import demo.entity.Photo;
import demo.service.PhotoService;


@RestController
@RequestMapping(value="api/photos")
public class PhotoController {

	
	@Autowired
	private PhotoService photoService;
	
	@GetMapping("/all")
	public ResponseEntity<List<PhotoDTO>> findAll(){
		List<PhotoDTO> photosDTO = new ArrayList<PhotoDTO>();
		List<Photo> photos = photoService.findAll();
		for(Photo a : photos)
			photosDTO.add(new PhotoDTO(a));
		return new ResponseEntity<List<PhotoDTO>>(photosDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<PhotoDTO> getOne(@PathVariable Long id){
		Photo photo = photoService.getOne(id);
		if(photo == null) {
			System.out.println("Photo not found.");
			return new ResponseEntity<PhotoDTO>(HttpStatus.NOT_FOUND);
		}
		PhotoDTO photoDTO = new PhotoDTO(photo);
		return new ResponseEntity<PhotoDTO>(photoDTO, HttpStatus.OK);
	}
	
	
	@PostMapping(value="/save",consumes="application/json")
	public ResponseEntity<PhotoDTO> save(@RequestBody PhotoDTO photoDTO){
		Photo photo = new Photo(photoDTO);
		photo = photoService.save(photo);
		return new ResponseEntity<PhotoDTO>(new PhotoDTO(photo), HttpStatus.CREATED);	
	}
	
	
	
	@PutMapping(consumes="application/json")
	public ResponseEntity<PhotoDTO> update(@RequestBody PhotoDTO photoDTO){
		Photo photo = photoService.getOne(photoDTO.getId()); 
		if (photo == null) {
			return new ResponseEntity<PhotoDTO>(HttpStatus.BAD_REQUEST);
		}
		photo.setPath(photoDTO.getPath());
		//photo.setContact(new Contact(photoDTO.getContact()));
		
		photo = photoService.save(photo);
		return new ResponseEntity<PhotoDTO>(new PhotoDTO(photo), HttpStatus.OK);	
	}
	
	@DeleteMapping("/{id}")	public ResponseEntity<Void> delete(@PathVariable Long id){
		Photo photo = photoService.getOne(id);
		if (photo != null){
			photoService.delete(photo);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
}
