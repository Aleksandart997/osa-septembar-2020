package demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import demo.dto.AccountDTO;
import demo.dto.FolderDTO;
import demo.dto.MessageDTO;
import demo.entity.Account;
import demo.entity.Folder;
import demo.entity.Message;
import demo.entity.User;
import demo.service.AccountService;

@Controller
@RequestMapping(value="api/accounts")
public class AccountController {

	
	@Autowired
	private AccountService accountService;
	
	@GetMapping
	public ResponseEntity<List<AccountDTO>> findAll(){
		List<AccountDTO> accountsDTO = new ArrayList<AccountDTO>();
		List<Account> accounts = accountService.findAll();
		for(Account a : accounts)
			accountsDTO.add(new AccountDTO(a));
		
		ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("index");
	    //return modelAndView;
		return new ResponseEntity<List<AccountDTO>>(accountsDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<AccountDTO> getOne(@PathVariable Long id){
		Account account = accountService.getOne(id);
		if(account == null) {
			System.out.println("Account not found.");
			return new ResponseEntity<AccountDTO>(HttpStatus.NOT_FOUND);
		}
		AccountDTO accountDTO = new AccountDTO(account);
		return new ResponseEntity<AccountDTO>(accountDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{username}")
	public ResponseEntity<AccountDTO> findByUsername(@PathVariable String username){
		Account account = accountService.findByUsername(username);
		if(account == null) {
			System.out.println("Account not found.");
			return new ResponseEntity<AccountDTO>(HttpStatus.NOT_FOUND);
		}
		AccountDTO accountDTO = new AccountDTO(account);
		return new ResponseEntity<AccountDTO>(accountDTO, HttpStatus.OK);
	}
	
	
	@PostMapping(value="/save",consumes="application/json")
	public ResponseEntity<AccountDTO> save(@RequestBody AccountDTO accountDTO){
		Account account = new Account(accountDTO);
		account = accountService.save(account);
		return new ResponseEntity<AccountDTO>(new AccountDTO(account), HttpStatus.CREATED);	
	}
	
	
	
	@PutMapping(consumes="application/json")
	public ResponseEntity<AccountDTO> update(@RequestBody AccountDTO accountDTO){
		//a student must exist
		Account account = accountService.getOne(accountDTO.getId()); 
		if (account == null) {
			return new ResponseEntity<AccountDTO>(HttpStatus.BAD_REQUEST);
		}
		account.setUsername(accountDTO.getUsername());
		account.setPassword(accountDTO.getPassword());
		account.setDisplayname(accountDTO.getDisplayname());
		account.setInServerAddress(accountDTO.getInServerAddress());
		account.setInServerPort(accountDTO.getInServerPort());
		account.setInServerType(accountDTO.getInServerType());
		account.setSmtpAddress(accountDTO.getSmtpAddress());
		account.setSmtpPort(accountDTO.getSmtpPort());
		List<Folder> folders = new ArrayList<Folder>();
		for(FolderDTO f : accountDTO.getFolders()) {
			Folder fd = new Folder(f);
			folders.add(fd);
		}
		account.setFolders(folders);
		List<Message> messages = new ArrayList<Message>();
		for(MessageDTO md : accountDTO.getMessages()) {
			Message m = new Message(md);
			messages.add(m);
		}
		account.setMessages(messages);
		account.setUser(new User(accountDTO.getUser()));
		
		
		account = accountService.save(account);
		return new ResponseEntity<AccountDTO>(new AccountDTO(account), HttpStatus.OK);	
	}
	
	@DeleteMapping("/{id}")	public ResponseEntity<Void> delete(@PathVariable Long id){
		Account account = accountService.getOne(id);
		if (account != null){
			accountService.delete(account);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
