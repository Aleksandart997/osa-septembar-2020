package demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.dto.AccountDTO;
import demo.dto.FolderDTO;
import demo.dto.MessageDTO;
import demo.entity.Account;
import demo.entity.Folder;
import demo.entity.Message;
import demo.entity.User;
import demo.service.MessageService;

@RestController
@RequestMapping(value="api/messages")
public class MessageController {
	
	@Autowired
	private MessageService messageService;
	
	@GetMapping("/all")
	public ResponseEntity<List<MessageDTO>> findAll(){
		List<MessageDTO> messagesDTO = new ArrayList<MessageDTO>();
		List<Message> messages = messageService.findAll();
		for(Message m : messages)
			messagesDTO.add(new MessageDTO(m));
		return new ResponseEntity<List<MessageDTO>>(messagesDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<MessageDTO> getOne(@PathVariable Long id){
		Message message = messageService.getOne(id);
		if(message == null) {
			System.out.println("Message not found.");
			return new ResponseEntity<MessageDTO>(HttpStatus.NOT_FOUND);
		}
		MessageDTO messageDTO = new MessageDTO(message);
		return new ResponseEntity<MessageDTO>(messageDTO, HttpStatus.OK);
	}
	
	
	@DeleteMapping("/{id}")	public ResponseEntity<Void> delete(@PathVariable Long id){
		Message message = messageService.getOne(id);
		if (message != null){
			messageService.delete(message);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
