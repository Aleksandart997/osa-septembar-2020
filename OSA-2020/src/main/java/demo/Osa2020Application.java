package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Osa2020Application {

	public static void main(String[] args) {
		SpringApplication.run(Osa2020Application.class, args);
	}

}
